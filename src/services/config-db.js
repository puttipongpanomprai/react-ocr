import { initializeApp } from "firebase/app";
import {getFirestore} from 'firebase/firestore'
import {getStorage} from 'firebase/storage'

const firebaseConfig = {
  apiKey: "AIzaSyAnQjT0TE9owArQxEzFWhbqHFHcQR9JIwA",
  authDomain: "react-ocr-6699c.firebaseapp.com",
  projectId: "react-ocr-6699c",
  storageBucket: "react-ocr-6699c.appspot.com",
  messagingSenderId: "539880367783",
  appId: "1:539880367783:web:33589be214c6ef1b8fdd82",
  measurementId: "G-5Y0QY3487Z"
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app)
const storage = getStorage(app)

export {db,storage}