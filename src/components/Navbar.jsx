import { Box, Container, Flex, Text } from "@chakra-ui/react";
import React from "react";
import { Link } from "react-router-dom";

function Navbar() {
  return (
    <Container maxW={"container.lg"} bg={"yellow.100"} p={5}>
      <Flex justifyContent={"space-between"}>
        <Box>
          <Text>LOGO</Text>
        </Box>
        <Box>
          <Flex justifyContent={'space-between'}>

          <Text>
            <Link to={'/'}>home</Link>
          </Text>
          <Text mx={5}>
            <Link to={'/detect'}>detect</Link>
          </Text>
          </Flex>
        </Box>
      </Flex>
    </Container>
  );
}

export default Navbar;
