import { Container } from '@chakra-ui/react'
import React from 'react'
import Navbar from './components/Navbar'
import { Route, Routes } from 'react-router-dom'
import Home from './pages/Home'
import Detect from './pages/Detect'

function App() {
  return (
    <Container maxW={'container.lg'}>
      <Navbar />
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/detect' element={<Detect />} />
      </Routes>
    </Container>
  )
}

export default App