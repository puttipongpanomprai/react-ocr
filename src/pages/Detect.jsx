import {
  Box,
  Button,
  Center,
  Container,
  FormControl,
  FormLabel,
  Text,
  Textarea,
} from "@chakra-ui/react";
import React, { useState } from "react";
import { Controller, useForm } from "react-hook-form";

function Detect() {
  const [way, setWay] = useState();
  const { handleSubmit, control } = useForm();
  const onSubmit = (data) => {
    const detectWord = /\s([\w\d]+)-([\w\d]+)-([\w\d]+)-([\w\d]+)-([\d]+)(:)([\d]+)-([\w\d]+)/g;
    const result = data.keyword.match(detectWord);
    if (result) {
      const pathWay = result[0].split(" ");
      setWay(pathWay[1]);
    }
  };
  return (
    <Container maxW={"container.md"}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Controller
          name="keyword"
          control={control}
          defaultValue={""}
          render={({ field: { value, onChange } }) => (
            <FormControl>
              <FormLabel>Keyword*</FormLabel>
              <Textarea value={value} onChange={onChange} />
            </FormControl>
          )}
        />
        <Center>
          <Button my={2} type="submit" colorScheme="teal">
            Detect
          </Button>
        </Center>
      </form>
      <Box>ชื่อเส้นทาง :{way && <Text>{way}</Text>}</Box>
    </Container>
  );
}

export default Detect;
