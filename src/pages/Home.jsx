import {
  Box,
  Button,
  Center,
  Container,
  Flex,
  FormControl,
  Heading,
  Image,
  Input,
  Spinner,
  Text,
  Textarea,
  useToast,
} from "@chakra-ui/react";
import React, { useEffect, useState } from "react";
// import Tesseract from "tesseract.js";
import axios from "axios";
import { storage } from "../services/config-db";
import { getDownloadURL, ref, uploadBytes } from "firebase/storage";
import { Controller, useForm } from "react-hook-form";
import copy from "copy-to-clipboard";
import { FiCopy } from "react-icons/fi";

function Home() {
  const [img, setImg] = useState();
  const [currentImg, setCurrentImg] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [texts, setTexts] = useState();
  const storageRef = ref(storage, "imgs1.jpeg");
  const { handleSubmit, control, setValue } = useForm();
  const toast = useToast();

  // functions
  const onConvert = async () => {
    console.log(img);
    setIsLoading(true);
    const metaData = {
      contentType: img.type,
    };
    await uploadBytes(storageRef, img, metaData).then((snapshot) => {
      console.log("Upload success : ", snapshot);
    });

    await getDownloadURL(ref(storage, storageRef)).then((url) => {
      setCurrentImg(url);
      axios
        .post("http://127.0.0.1:5001/react-ocr-6699c/us-central1/convertImg", {
          url,
        })
        .then((res) => {
          const detectWord = /\s([\w\d]+)-([\w\d]+)-([\w\d]+)-([\w\d]+)-([\d]+)(:)([\d]+)-([\w\d]+)/g;
          const result = res.data.match(detectWord);
          // console.log(result[0]);
          setTexts(result[0]);
        });
    });
  };
  const copyToClipbord = (data) => {
    const text = data.copy;
    let isCopy = copy(text);
    if (isCopy) {
      toast({
        title: "Copy",
        description: "Copy to clipbord",
        status: "success",
        duration: 3000,
        isClosable: true,
      });
    }
  };

  useEffect(() => {
    setIsLoading(false);
    setValue("copy", texts);
  }, [texts]);
  const changed = (e) => {
    setImg();
    setTexts();
    setCurrentImg();
    let img = e.target.files;
    setImg(img[0]);
  };
  return (
    <Container maxW={"container.md"}>
      <Center>
        <Box>
          <Heading>OCR</Heading>
        </Box>
      </Center>
      <Center>
        <Flex flexDirection={"column"} >
          <Box maxW={"50%"}>
            <Input type="file" multiple onChange={changed} />
          </Box>
          <Box>{currentImg && <Image src={currentImg} />}</Box>
          <Box>
            <Center>
              <Button
                isLoading={isLoading}
                onClick={onConvert}
                my={2}
                colorScheme="blue"
              >
                Convert
              </Button>
            </Center>
          </Box>
          {texts && !isLoading ? (
              <form onSubmit={handleSubmit(copyToClipbord)}>
                <Box textAlign={'right'}>

                <Button type="submit" bg={'#fff'} _hover={{opacity:1}}><FiCopy/></Button>
                </Box>
                <Controller
                  name="copy"
                  control={control}
                  render={({ field: { value } }) => (
                    <FormControl h='500px'>
                      <Textarea h={'100%'} isReadOnly value={value} />
                    </FormControl>
                  )}
                />
              </form>
          ) : isLoading ? (
            <Center>
              <Spinner size={"md"} />
            </Center>
          ) : (
            ""
          )}
        </Flex>
      </Center>
    </Container>
  );
}

export default Home;
