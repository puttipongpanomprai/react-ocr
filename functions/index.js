const functions = require("firebase-functions");
const admin = require("firebase-admin");
const cors = require("cors");
const tess = require("node-tesseract-ocr");

admin.initializeApp();

exports.convertImg = functions.https.onRequest((req, res) => {
  cors()(req, res, async () => {
    const {url} = req.body
    const config = {
      lang: 'tha+eng',
      oem:1,
      psm:3
      };
    await tess
      .recognize(url, config)
      .then((text) => {
        console.log(text)
        res.send(text)
      })
      .catch((error) => {
        res.send(error)
      });
  });
});
